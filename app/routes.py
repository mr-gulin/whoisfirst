from app import app
from flask import jsonify
from flask import request

first_list = []


@app.route('/api/get')
def index():
    return jsonify(first_list)


@app.route('/api/add', methods=['POST'])
def add_first():
    first_list.append(request.json['name'])
    return jsonify(first_list)


@app.route('/api/clear', methods=['POST'])
def reset():
    first_list.clear()
    return jsonify(first_list)
