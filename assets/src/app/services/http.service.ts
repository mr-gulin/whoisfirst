import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable()
export class HttpService {
    private baseUrl = environment.baseUrl;

    constructor(private http: HttpClient) { }


    get() {
        return this.http.get(this.baseUrl + '/get');
    }

    add(name: string) {
        return this.http.post(this.baseUrl + '/add', {name});
    }

    clear() {
        return this.http.post(this.baseUrl + '/clear', {});
    }
}
