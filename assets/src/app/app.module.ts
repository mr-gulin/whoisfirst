import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {HttpService} from './services/http.service';
import {CookieService} from 'ngx-cookie-service';

// Components
import { MasterViewComponent } from './components/master-view/master-view.component';
import { MainViewComponent } from './components/main-view/main-view.component';
import { PlayerViewComponent } from './components/player-view/player-view.component';

@NgModule({
    declarations: [
        AppComponent,
        MasterViewComponent,
        MainViewComponent,
        PlayerViewComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        AppRoutingModule,
        HttpClientModule
    ],
    providers: [HttpService, CookieService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
