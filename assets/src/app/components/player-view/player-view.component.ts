import {Component, OnInit} from '@angular/core';
import {HttpService} from '../../services/http.service';
import {CookieService} from 'ngx-cookie-service';

const CookieNameAttribute = 'name';

@Component({
    selector: 'app-player-view',
    templateUrl: './player-view.component.html',
    styleUrls: ['./player-view.component.less']
})
export class PlayerViewComponent implements OnInit {
    public name;
    private timeout;
    private isButtonDisabled;

    constructor(private httpService: HttpService, private cookieService: CookieService) {
    }

    ngOnInit() {
        this.isButtonDisabled = false;
        const name = this.cookieService.get(CookieNameAttribute);

        this.name = name || '';
        this.fetchData();
    }

    isButtonEnabled() {
        return this.name && this.name !== '' && !this.isButtonDisabled;
    }

    fetchData() {
        this.httpService.get().subscribe((res: any[]) => {
            clearTimeout(this.timeout);
            if (res && !res.length) {
                this.isButtonDisabled = false;

                return;
            }

            this.isButtonDisabled = true;

            this.timeout = setTimeout(() => {
                this.fetchData();
            }, 500);
        });
    }

    play() {
        this.isButtonDisabled = true;

        this.httpService.add(this.name).subscribe((res: any[]) => {
            if (!res.length) {
                return;
            }
            this.isButtonDisabled = true;

            this.timeout = setTimeout(() => {
                this.fetchData();
            });
        });
    }

    onInputChange(event) {
        console.log(CookieNameAttribute);
        this.cookieService.set(CookieNameAttribute, this.name);
    }

}
