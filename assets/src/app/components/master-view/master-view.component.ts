import {Component, OnDestroy, OnInit} from '@angular/core';
import {HttpService} from '../../services/http.service';

@Component({
    selector: 'app-master-view',
    templateUrl: './master-view.component.html',
    styleUrls: ['./master-view.component.less']
})
export class MasterViewComponent implements OnInit, OnDestroy {
    public userList;
    private timeout;

    constructor(private httpService: HttpService) {

    }

    ngOnInit() {
        this.get();
    }

    clear() {
        this.httpService.clear().subscribe(() => {

        });
    }

    get() {
        this.httpService.get().subscribe((res: any) => {
            this.userList = res;
            clearTimeout(this.timeout);
            this.timeout = setTimeout(() => {
                this.get();
            }, 500);
        });
    }

    ngOnDestroy() {
        clearTimeout(this.timeout);
    }

}
