import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
    selector: 'app-main-view',
    templateUrl: './main-view.component.html',
    styleUrls: ['./main-view.component.less']
})
export class MainViewComponent implements OnInit {

    constructor(private router: Router) {
    }

    ngOnInit() {
    }

    onMasterClick() {
        this.router.navigate(['master']);
    }

    onPlayerClick() {
        this.router.navigate(['player']);
    }
}
