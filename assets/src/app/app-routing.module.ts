import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {MasterViewComponent} from './components/master-view/master-view.component';
import {MainViewComponent} from './components/main-view/main-view.component';
import {PlayerViewComponent} from './components/player-view/player-view.component';


const routes: Routes = [
    {path: '', component: MainViewComponent},
    {path: 'master', component: MasterViewComponent},
    {path: 'player', component: PlayerViewComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
